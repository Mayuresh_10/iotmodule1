# Basics of Industrial IoT

## WHAT IS IOT?
Internet of things can be defined as multiple devices transferring information to one another through a common network called the internet, without any human-to-human or human-to-computer intervention.
“Things” is referred to any device that can be given some computational power and can be assigned an internet protocol (IP) address. 

## WHAT IS IIOT?
* The industrial internet of things (IIoT) refers to the extension and use of the internet of things (IoT) in industrial sectors and applications. With a strong focus on machine-to-machine (M2M) communication, big data, and machine learning, the IIoT enables industries and enterprises to have better efficiency and reliability in their operations.
* The connectivity with the Internet allows for data collection, exchange, and analysis, potentially facilitating improvements in productivity and efficiency as well as other economic benefits.

## Industrial Revoution


* Industry 1.0 - Mechanization, Steam Power, Weaving loom.
* Industry 2.0 - Mass production, Assembly lines, Electrical energy.
* Industry 3.0 - Automation, Computers and Electronics,Information Technology.
* Industry 4.0 - Cyber Physical Systems, Internet of Things, Networking.

![](https://www.netobjex.com/wp-content/uploads/2018/12/1-1.png)

## INDUSTRY 3.0 ARCHITECTURE AND COMMUNICATION PROTOCOLS

* The third industrial revolution brought semiconductors, mainframe computing, personal computing, and the Internet—the digital revolution. Things that used to be analog moved to digital technologies, like an old television you used to tune in with an antenna (analog) being replaced by an Internet-connected tablet that lets you stream movies (digital).

* The move from analog electronic and mechanical devices to pervasive digital technology dramatically disrupted industries, especially global communications and energy. Electronics and information technology began to automate production and take supply chains global.

**_Sensors --> PLC's --> SCADA & ERP_**
![](https://www.sec.gov/Archives/edgar/data/1357450/000114420419018681/tv518271_chrt-industrial.jpg)

* Sensors installed at various points in the Factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data.
* Usually this data is stored in Excels and CSV's and rearely get plotted as real-time graphs or charts.

### COMMUNICATION PROTOCOLS

All the protocols used in Industry 3.0 are optimized for sending data to a central server inside the factory.
These protocols are used by sensors to Send data to PLC's. These protocols are called as fieldbus.
A few protocols are listed below:-
1. MODBUS
2. CanOpen
3. ETHERCat
4. PROFI NET

## Industry 4.0 ARCHITECTURE AND COMMUNICATION PROTOCOLS

**_Industry 4.0 is Industry 3.0 connected to Internet, which is called IoT._**

Major advantages of connecting the industrial devices to the internet is that you can collect lots of machine data and can analyse machine performance, characteristics and the Internet makes data communication faster, easier without data loss

**With the help of the data we can provide multiple features to the user like:-**
1. Showing Dashboards
2. Remote Web SCADA
3. Remote control configuration of devices.
4. Predictive maintenance.
5. Real-time event processing.
6. Analytics.
7. Real-time alerts & alarms.

### Architecture

* IoT Gateway acts as a bridge between PLCs and SCADA, Converts the data received from them and send to Cloud(translated output for cloud)
* Edge devices gets the data from controllers and convert it to a cloud understandable protocol.

![](https://user-content.gitlab-static.net/8e9992cbb3dba582028b932fa81d3b79dfd89ab4/68747470733a2f2f696d6167652e736c696465736861726563646e2e636f6d2f6f70656e736f75726365666f72696e647573747279342d3137313132303136353933312f39352f6f70656e2d736f757263652d736f6674776172652d666f722d696e6475737472792d34302d362d3633382e6a70673f63623d31353131323034313935)

### COMMUNICATION PROTOCOLS

**Some Protocols used in industry 4.0 are-**
1. MQTT.org
2. AMQP
3. OPC UA
4. CoAP RFC 7252
5. HTTP
6. WebSocket

## Problems with Industry 4.0 upgrades 
**Cost-** Industry 3.0 devices are very Expensive as a factory owner I dont want to switch to Industry 4.0 devices because they are Expensive.

**Downtime-** Changing Hardware means a big factory downtime, I dont want to shut down my factory to upgrade devices.

**Reliablity-** I dont want to invest in devices which are unproven and unreliable.

### Solutions:

* We can easily Get data from the devices already present in the factory just by adding a few extra sensors to the devices already present and send the data to the cloud.
* So, there is no need to replace the original devices, causing lower risk factor.
* Industry 3.0 Protocols needs to be changed to Industry 4.0 Protocols, but that can be done by arious libraries.

**However some challenges that are still faced:**
1. Expensive Hardware
2. Lack of documentation
3. Propritary PLC Protocols

## Steps to make an IIoT Product

* Identify most popular Industry 3.0 devices.
* Study Protocols that these devices Communicate.
* Get data from the Industry 3.0 devices.
* Send the data to cloud for Industry 4.0.
* Store your data in Time series databases i.e. all the data that we recieve is stored in the database with a timestamp.
* View all your data into beautiful dashboards for better visualisation and insights through graphs pie charts and various other widgets by using tools like Thingsboard, influxDB, Grafana etc.
* After sending data to cloud, Data Analysis is carried out with the help of several IOT Platforms like AWS IOT, Google IOT, Azure IOT etc.
* Get alerts, alarms and notifications through email or mobile apps when a pirticular setpoint is reached using services like IFTTT, Zaiper, twilio.

![](https://i.pinimg.com/originals/87/ba/29/87ba2951994a4ad91f499b763d408ed9.png)




